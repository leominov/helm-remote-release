# helm-remote-release

WIP.

## Usage

```shell
Usage:
  helm-remote-release [command]

Available Commands:
  build       build charts/ directory
  completion  generate the autocompletion script for the specified shell
  fetch       fetch Chart configuration from Kubernetes cluster
  help        Help about any command
  template    locally render templates

Flags:
  -h, --help   help for helm-remote-release

Use "helm-remote-release [command] --help" for more information about a command.
```

## Configuration

### ArgoCD ConfigMap

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-cm
data:
  configManagementPlugins: |
    - name: helm-remote-release
      init:
        command: ["/bin/sh", "-c"]
        args: ["helm-remote-release fetch --build --cleanup --dependency-version-map=app-template=^1.0.0 --namespace=$ARGOCD_APP_NAMESPACE $ARGOCD_APP_NAME"]
      generate:
        command: ["/bin/sh", "-c"]
        args: ["helm-remote-release template --namespace=$ARGOCD_APP_NAMESPACE $ARGOCD_APP_NAME"]
```

### ArgoCD Application

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  project: default
  destination:
    server: https://kubernetes.default.svc
    namespace: app
  source:
    repoURL: git@gitlab.local:my/repo.git
    targetRevision: HEAD
    path: "helm"
    plugin:
      name: helm-remote-release
      env:
        - name: DEPENDENCY_VERSION_MAP_1
          value: '{"redis":"^1.0.0"}'
        - name: DEPENDENCY_VERSION_MAP_2
          value: '{"postgresql":"^1.0.0"}'
```
