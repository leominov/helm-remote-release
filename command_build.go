package main

import (
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

func buildCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "build",
		Short: "build charts/ directory",
		Long:  "build charts/ directory, alias for `helm dependency build` command",
		RunE: func(cmd *cobra.Command, args []string) error {
			return runBuildCommand()
		},
	}
	return cmd
}

func runBuildCommand() error {
	command := exec.Command("helm", "dependency", "build")
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	return command.Run()
}
