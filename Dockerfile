FROM golang:1.16-alpine3.13 AS builder
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/helm-remote-release
COPY . .
RUN CGO_ENABLED=0 go build .

FROM quay.io/argoproj/argocd:v2.2.5
COPY --from=builder /go/src/gitlab.qleanlabs.ru/platform/infra/helm-remote-release/helm-remote-release /usr/bin/
