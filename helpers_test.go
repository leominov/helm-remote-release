package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsFileExists(t *testing.T) {
	assert.True(t, IsFileExists("main.go"))
	assert.False(t, IsFileExists("foobar.go"))
}
