package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/yaml"
)

const (
	ChartFilename            = "Chart.yaml"
	ChartsDirectory          = "charts"
	RequirementsLockFilename = "requirements.lock"
	ValuesFilename           = "values.yaml"
	DependencyMapLevels      = 5
)

type fetchCommandOptions struct {
	ReleaseName          string
	Namespace            string
	Build                bool
	Cleanup              bool
	Debug                bool
	DependencyVersionMap map[string]string
}

func fetchCommand() *cobra.Command {
	opts := &fetchCommandOptions{}
	cmd := &cobra.Command{
		Use:   "fetch [NAME]",
		Short: "fetch Chart configuration from Kubernetes cluster",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.ReleaseName = args[0]
			opts.DependencyVersionMap = viper.GetStringMapString("dependency-version-map")

			for i := 1; i <= DependencyMapLevels; i++ {
				configParam := "dependency-version-map-" + strconv.Itoa(i)
				for k, v := range viper.GetStringMapString(configParam) {
					opts.DependencyVersionMap[k] = v
				}
			}

			if opts.Debug {
				fmt.Printf("Configuration: %s\n", opts.String())
			}

			cli, err := NewClientSet()
			if err != nil {
				return err
			}

			return runFetchCommand(cli, opts)
		},
	}
	flags := cmd.PersistentFlags()
	flags.BoolVar(&opts.Build, "build", false, "Build charts/ directory")
	flags.BoolVar(&opts.Debug, "debug", false, "Prints configuration before")
	flags.BoolVar(&opts.Cleanup, "cleanup", false, "Remove previously fetched data")
	flags.StringVarP(&opts.Namespace, "namespace", "n", "", "Helm Release namespace")

	flags.StringToString("dependency-version-map", nil, "Specify version for Chart dependencies")
	_ = viper.BindPFlag("dependency-version-map", flags.Lookup("dependency-version-map"))
	_ = viper.BindEnv("dependency-version-map", "DEPENDENCY_VERSION_MAP")

	for i := 1; i <= DependencyMapLevels; i++ {
		configParam := "dependency-version-map-" + strconv.Itoa(i)
		envParam := "DEPENDENCY_VERSION_MAP_" + strconv.Itoa(i)
		flags.StringToString(configParam, nil, "Specify version for Chart dependencies (additional)")
		_ = flags.MarkHidden(configParam)
		_ = viper.BindPFlag(configParam, flags.Lookup(configParam))
		_ = viper.BindEnv(configParam, envParam)
	}
	return cmd
}

func (f *fetchCommandOptions) String() string {
	b, _ := json.Marshal(f)
	return string(b)
}

func runFetchCommand(cli kubernetes.Interface, opts *fetchCommandOptions) error {
	if opts.Cleanup {
		for _, dest := range []string{
			ChartFilename,
			ChartsDirectory,
			RequirementsLockFilename,
			ValuesFilename,
		} {
			_ = os.RemoveAll(dest)
		}
	}

	release, err := GetHelmRelease(cli, opts.Namespace, opts.ReleaseName)
	if err != nil {
		return err
	}

	for _, dep := range release.Chart.Metadata.Dependencies {
		for name, ver := range opts.DependencyVersionMap {
			if dep.Name == name {
				dep.Version = ver
			}
		}
	}

	meta, err := yaml.Marshal(release.Chart.Metadata)
	if err != nil {
		return err
	}
	err = os.WriteFile(ChartFilename, meta, 0644)
	if err != nil {
		return err
	}
	config, err := yaml.Marshal(release.Config)
	if err != nil {
		return err
	}
	err = os.WriteFile(ValuesFilename, config, 0644)
	if err != nil {
		return err
	}

	if opts.Build {
		return runBuildCommand()
	}

	return nil
}
