package main

import (
	"errors"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func init() {
	cobra.OnInitialize(func() {
		viper.AutomaticEnv()
	})
}

func main() {
	var rootCmd = &cobra.Command{
		Use:          "helm-remote-release",
		SilenceUsage: true,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			_, err := exec.LookPath("helm")
			if err != nil {
				return errors.New("failed to find helm binary")
			}
			return nil
		},
	}
	rootCmd.AddCommand(
		buildCommand(),
		fetchCommand(),
		templateCommand(),
	)
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
