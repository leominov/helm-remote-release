package main

import (
	"context"
	"fmt"

	"helm.sh/helm/v3/pkg/release"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
)

func NewClientSet() (*kubernetes.Clientset, error) {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, &clientcmd.ConfigOverrides{})
	config, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func GetHelmRelease(clientset kubernetes.Interface, namespace, releaseName string) (*release.Release, error) {
	releases, err := ListHelmReleases(clientset, namespace)
	if err != nil {
		return nil, err
	}
	for _, rls := range releases {
		if rls.Name == releaseName {
			return rls, nil
		}
	}
	return nil, fmt.Errorf("release %q not found", releaseName)
}

func ListHelmReleases(clientset kubernetes.Interface, namespace string) ([]*release.Release, error) {
	secretList, err := clientset.CoreV1().Secrets(namespace).List(context.Background(), metav1.ListOptions{
		FieldSelector: "type=helm.sh/release.v1",
		LabelSelector: "status=deployed",
	})
	if err != nil {
		return nil, err
	}
	var releases []*release.Release
	for _, secret := range secretList.Items {
		releaseRaw, ok := secret.Data["release"]
		if !ok {
			continue
		}
		rls, err := DecodeRelease(string(releaseRaw))
		if err != nil {
			continue
		}
		releases = append(releases, rls)
	}
	return releases, nil
}
