package main

import (
	"errors"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

type templateCommandOptions struct {
	ReleaseName string
	Namespace   string
	Debug       bool
}

func templateCommand() *cobra.Command {
	opts := &templateCommandOptions{}
	cmd := &cobra.Command{
		Use:   "template [NAME]",
		Short: "locally render templates",
		Long:  "locally render templates, alias for `helm template` command",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.ReleaseName = args[0]
			return runTemplateCommand(opts)
		},
	}
	flags := cmd.PersistentFlags()
	flags.StringVarP(&opts.Namespace, "namespace", "n", "", "Helm Release namespace")
	return cmd
}

func runTemplateCommand(opts *templateCommandOptions) error {
	command := exec.Command("helm", "template", opts.ReleaseName, ".", "--namespace", opts.Namespace)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	err := command.Run()
	return err
}
